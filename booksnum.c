/*
    booksnum.c
    A fajlban talalhato fuggveny visszaadja egy tomb elemeinek a szamat.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#include "customtypes.h"

/*
    Feladata: meghatarozni egy tomb nem ures elemeinek szamat.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: konyv tipusu tomb, illetve annak hossza a tombben levo nem ures elemek meghatarozasahoz.
    Kimeno parameterek (visszateresi ertekek): nem ures elemek szama, ami egy nemnegativ egesz szam.
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: -
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
int
konyvek_Szama( konyv *konyvek, int len )
{
    int counter = 0;
    /*
            Megszamolja a tomb nem ures elemeit, 0-tol indulva addig, amig veget nem ert a tombon (minden eleme fel van toltve),
            vagy megtalalja a tomb elso ures elemet jelzo 0-t.
    */
    for( int i = 0; i < len && konyvek[i].id != 0; i++ )
    {
        counter++;
    }
    return counter;
}
