/*
    search.c
    A fajlban talalhato fuggvenyek keresest hajtanak vegre alkalmazva az eldontes, a kivalasztas, illetve a szelsoertek-kivalasztas
    teteleit.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#include <stdio.h>
#include <string.h>
#include "customtypes.h"

/*
    Feladata: egy konyv tipusu elemebol kiiratja a kepernyore a tomb ev mezojeben a legnagyobb szamot tartalmazo elem osszes mezojet.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb es elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
kereses_LegujabbKonyv( konyv *konyvek, int len )
{
    int legujabbPoz = konyvek[0].id;
    int legujabbEv = konyvek[0].ev;
/*
    Ellenorzi hogy az adott tombelem ev mezojeben levo szam nagyobb-e a legujabbEv-ben taroltnal. Ha nagyobb, akkor az lesz
    a legujabbEv valtozo uj erteke, a ciklusvaltozo  pedig a legujabbPoz erteke.
*/
    for( int i = 0; i < len && konyvek[i].id != 0 ; i++ )
    {
        if ( konyvek[i].ev > legujabbEv )
        {
            legujabbPoz = konyvek[i].id;
            legujabbEv = konyvek[i].ev;
        }
    }
    legujabbPoz--;
    printf( "A legujabb konyv: %d %s %s %d\n", konyvek[legujabbPoz].id, konyvek[legujabbPoz].cim, konyvek[legujabbPoz].szerzo, konyvek[legujabbPoz].ev );
}

/*
    Feladata: egy konyv tipusu elemebol kiiratja a kepernyore a tomb ev mezojeben a legkisebb szamot tartalmazo elem osszes mezojet.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb es elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
kereses_LegregibbKonyv( konyv *konyvek, int len )
{
    int legregebbiPoz = konyvek[0].id;
    int legregebbiEv = konyvek[0].ev;
/*
    Ellenorzi hogy az adott tombelem ev mezojeben levo szam kisebb-e a legregebbiEv-ben taroltnal. Ha kisebb, akkor az lesz
    a legregebbiEv erteke, a ciklusvaltozo  pedig a legregebbiEv erteke.
*/
    for( int i = 0; i < len && konyvek[i].id != 0 ; i++ )
    {
        if ( konyvek[i].ev < legregebbiEv )
        {
            legregebbiPoz = konyvek[i].id;
            legregebbiEv = konyvek[i].ev;
        }
    }
    legregebbiPoz--; //a tombelem tombindexe eggyel kisebb lesz az azonositojatol.
    printf( "A legregebbi konyv: %d %s %s %d\n", konyvek[legregebbiPoz].id, konyvek[legregebbiPoz].cim, konyvek[legregebbiPoz].szerzo, konyvek[legregebbiPoz].ev );
}

/*
    Feladata: egy konyv tipusu tombbol azon elemek kiiratasa a kepernyore, amelyek tartalmazzak a felhasznalo altal megadott
    karaktersorozatot a cim mezoben.

    Hasznalatanak elofeltetelei: egy konyv tipusu tomb dekralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: felhasznalo altal megadott karaktersorozat.
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: scanf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
kereses_CimSzerint( konyv *konyvek, int len )
{
    char keresettCim[255];
    int i = 0;
    int talalatokSzama = 0;
    printf(" Adja meg a keresett konyv cimet:\n" );
    scanf( "%s", keresettCim );
/*
        Ellenorzi, hogy az adott tombelem cim mezojenek ertekeben megtalalhato-e a felhasznalo altal megadott karaktersorozat.
        Ha igen, akkor kiiratja az elem osszes mezojenek erteket. Addig fut, amig az id mezoben az elso ures elemet jelzo 0-t, vagy a tomb
        veget el nem eri.
*/
    while( konyvek[i].id != 0 )
    {
        if ( strstr( konyvek[i].cim, keresettCim ))
           {
               printf( "%d %s %s %d\n", konyvek[i].id, konyvek[i].cim, konyvek[i].szerzo, konyvek[i].ev );
               talalatokSzama++;
           }
        i++;
    }
    /*
        Ha vannak talalatok, kiirja azok szamat.
    */
    if ( talalatokSzama == 0 )
    {
        printf( "Nincs talalat a kovetkezore: %s\n", keresettCim );
    }
        else
        {
            printf( "%d darab talat a kovetkezore: %s\n", talalatokSzama, keresettCim );
        }
}

/*
    Feladata: egy konyv tipusu tombbol azon elemek kiiratasa a kepernyore, amelyek tartalmazzak a felhasznalo altal megadott
    karaktersorozatot a szerzo mezoben.

    Hasznalatanak elofeltetelei: egy konyv tipusu tomb dekralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: felhasznalo altal megadott karaktersorozat.
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: scanf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
kereses_SzerzoSzerint( konyv *konyvek, int len )
{
    char keresettSzerzo[255];
    int i = 0;
    int talalatokSzama = 0;
    printf( "Adja meg a keresett konyv szerzojet:\n" );
    scanf( "%s", keresettSzerzo );
/*
        Ellenorzi, hogy az adott tombelem szerzo mezojenek ertekeben megtalalhato-e a felhasznalo altal megadott karaktersorozat.
        Ha igen, akkor kiiratja az elem osszes mezojenek erteket. Addig fut, amig az id mezoben az elso ures elemet jelzo 0-t, vagy a tomb
        veget el nem eri.
*/
    while( konyvek[i].id != 0 )
    {
        if ( strstr( konyvek[i].szerzo, keresettSzerzo ))
           {
               printf( "%d %s %s %d\n", konyvek[i].id, konyvek[i].cim, konyvek[i].szerzo, konyvek[i].ev );
               talalatokSzama++;
           }
        i++;
    }
    /*
        Ha vannak talalatok, kiirja azok szamat.
    */
    if ( talalatokSzama == 0 )
    {
        printf( "Nincs talalat a kovetkezore: %s\n", keresettSzerzo );
    }
        else
        {
            printf( "%d darab talat a kovetkezore: %s\n", talalatokSzama, keresettSzerzo );
        }
}

/*
    Feladata: egy konyv tipusu tombbol azon elemek kiiratasa a kepernyore, amelyek ev mezojenek erteke megegyezik a felhasznalo altal
    megadott egesz szammal.

    Hasznalatanak elofeltetelei: egy konyv tipusu tomb dekralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: felhasznalo altal megadott egesz szam.
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: scanf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
kereses_KiadasiEvSzerint( konyv *konyvek, int len )
{
    int keresettEv;
    printf( "Adja meg a keresett kiadasi evet:\n" );
    scanf( "%d", &keresettEv );
    /*
        Ellenorzi, hogy az adott tombelem ev mezojenek erteke megegyezik-e a felhasznalo altal megadott ertekkel. Ha igen,
        akkor kiiratja az elem osszes mezojenek erteket. Addig fut, amig az id mezoben az elso ures elemet jelzo 0-t, vagy a tomb
        veget el nem eri.
    */
    for( int i = 0; i < len && konyvek[i].id != 0 ; i++ )
    {
        if ( konyvek[i].ev == keresettEv )
        {
            printf( "%d %s %s %d\n", konyvek[i].id, konyvek[i].cim, konyvek[i].szerzo, konyvek[i].ev );
        }
    }
}
