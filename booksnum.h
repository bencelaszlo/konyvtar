/*
    booksnum.c
    A fajlban talalhato fuggveny visszaadja egy tomb elemeinek a szamat.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#ifndef _BOOKSNUM_H_
#define _BOOKSNUM_H_

/*
    Feladata: meghatarozni egy tomb nem ures elemeinek szamat.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: konyv tipusu tomb cime, illetve annak hossza a tombben levo nem ures elemek meghatarozasaohoz.
    Kimeno parameterek (visszateresi ertekek): nem ures elemek szama, ami egy nemnegativ egesz szam.
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: -
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    int
    konyvek_Szama( konyv *konyvek, int len );
#endif

