/*
    arrayhandler.h
    A fajlban talalhato fuggvenyek a tombok kezeleset vegzik.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/


#ifndef _ARRAYHANDLER_H_
#define _ARRAYHANDLER_H_
/*
    Feladata: egy konyv tipusu tomb osszes mezojenek kiiratasa a kepernyore.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf(), scanf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
tomb_Listazas( konyv *konyvek, int len );

/*
    Feladata: egy konyv tipusu tomb utolso nem ures eleme utan egy felhasznalo altal megadott uj elem beillesztese.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf(), scanf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
tomb_UjHozzadas( konyv *konyvek, int len );
#endif
