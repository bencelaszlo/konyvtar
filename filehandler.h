/*
    filehandler.h
    A fajlban talalhato fuggvenyek vegzik az adatok tarolasara hasznalt fajlbol valo beolvasast es az abba valo kiirast.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#ifndef _FILEHANDLER_H_
#define _FILEHANDLER_H_
/*
    Feladata: beolvassa a database.txt-ben talalhato adatokat, es feltolt vele egy konyv tipusu tombot.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb definialasa.
    Bemeno parameterek: egy konyv tipusu tomb, amelybe beolvashatja a fajlban levo adatokat.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: konyvek, amelyet feltolt ertekekkel.
    Hivatkozott globalis valtozok: konyvek.
    Hivott fuggvenyek: fopen(), fclose(), printf(), fscanf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    file_Read( konyv *konyvek );

/*
    Feladata: a database.txt-be adatok kiirasa egy konyv tipusu tombbol.
    Hasznalatanak elofeltetelei: egy feltoltott konyv tipusu tomb meglete.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: fopen(), fclose(), fprintf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    file_Write( konyv *konyvek, int len );
#endif
