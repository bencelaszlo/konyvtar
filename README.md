# konyvtar
Féléves beadandó feladat. (Miskolci Egyetem - Programozás alapjai (ANSI C)
A program célja, hogy egy könyvtárban megtalálható könyveket (azonosító, cím, szerző, kiadás éve) lehessen vele nyilvántartani.

##Funkciók
- könyvek kiírása konzolra
- könyvek számának kiírása konzolra
- tárolás szöveges fájlban
- keresés cím, szerző, kiadás éve szerint
- legújabb és legrégibb könyv megkeresése
