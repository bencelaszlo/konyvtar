/*
    arrayhandler.c
    A fajlban talalhato fuggvenyek a tombok kezeleset vegzik.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#include <stdio.h>
#include "customtypes.h"

/*
    Feladata: egy konyv tipusu tomb osszes mezojenek kiiratasa a kepernyore.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf(), scanf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
tomb_Listazas( konyv *konyvek, int len )
{
    /*
        Vegigmegy a tomb nem ures elemein es kiiratja annak mezoit.
    */
    for( int i = 0; i < len && konyvek[i].id != 0; i++ )
    {
        printf( "%d\t%s\n%s %d\n\n", konyvek[i].id, konyvek[i].szerzo, konyvek[i].cim, konyvek[i].ev );
    }
}

/*
    Feladata: egy konyv tipusu tomb utolso nem ures eleme utan egy felhasznalo altal megadott uj elem beillesztese.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf(), scanf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
tomb_UjHozzadas( konyv *konyvek, int len )
{
    int karakterPoz = 0;
    /*
        A felhasznalo altal megadando ertekeket tarolo valtozok.
    */
    int ujEv = 0;
    int ujId = 0;
    char ujSzerzo[255];
    char ujCim[255];
    /*
        Uj elem mezoinek bekerese a felhasznalotol.
    */
    printf( "Adja meg az uj konvv adatait a kovetkezo formatumban:\nszerzo neve konyv cime kiadas eve,\n" );
    scanf( "%s %s %d", ujSzerzo, ujCim, &ujEv );
    /*
        Megkeresi a tomb elso ures elemet, amelyet az id mezoben egy 0 jelez.
    */
    while( konyvek[ujId].id != 0 )
    {
        ujId++;
    }
    konyvek[ujId].id = ujId + 1; //az uj elem azonositoja (id) eggyel tobb lesz, mint a tombindexe (a 0-s az elso ures elemet jeloli.
    konyvek[ujId].ev = ujEv;
    /*
        Karakterenket atmasolja a tomb szerzo mezojebe az uj elem megfelelo valtozojat.
    */
    while( ujSzerzo[karakterPoz] != '0' )
    {
        konyvek[ujId].szerzo[karakterPoz] = ujSzerzo[karakterPoz];
        karakterPoz++;
    }
    /*
        Karakterpozicio visszaallitasa 0-ra, majd a tomb szerzo mezojebe az uj elem megfelelo valtozojanak masolasa karakterenkent.
    */
    karakterPoz = 0;
    while( ujCim[karakterPoz] != '0' )
    {
        konyvek[ujId].cim[karakterPoz] = ujCim[karakterPoz];
        karakterPoz++;
    }
}
