/*
    filehandler.c
    A fajlban talalhato fuggvenyek vegzik az adatok tarolasara hasznalt fajlbol valo beolvasast es az abba valo kiirast.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#include <stdio.h>
#include "customtypes.h"

/*
    Feladata: beolvassa a database.txt-ben talalhato adatokat, es feltolt vele egy konyv tipusu tombot.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb definialasa.
    Bemeno parameterek: egy konyv tipusu tomb, amelybe beolvashatja a fajlban levo adatokat.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: konyvek, amelyet feltolt ertekekkel.
    Hivatkozott globalis valtozok: konyvek.
    Hivott fuggvenyek: fopen(), fclose(), printf(), fscanf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void
file_Read( konyv *konyvek )
{
    int konyvPoz = 0;
    FILE *fp;

    fp = fopen( "database.txt", "r" );
    if (fp == NULL) //ellenorzi, hogy letezik-e a fajl.
    {
        printf( "Hiba a fajl megnyitsa kozben!\n" );
    }

    /*
    Elvegzi a fajlbol a sorok beolvasasat, illetve feltolti azokkal a tomb megfelelo elemeinek megfelelo mezoit addig, amig a fajl
    vegehez nem er.
    */
    while( !feof(fp) )
    {
        fscanf( fp, "%d %s %s %d\n", &konyvek[konyvPoz].id, konyvek[konyvPoz].cim, konyvek[konyvPoz].szerzo, &konyvek[konyvPoz].ev );
        konyvPoz++;
    }
    fclose( fp );
}

/*
    Feladata: a database.txt-be adatok kiirasa egy konyv tipusu tombbol.
    Hasznalatanak elofeltetelei: egy feltoltott konyv tipusu tomb meglete.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: fopen(), fclose(), fprintf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
void file_Write( konyv *konyvek, int len )
{
    int konyvPoz = 0;
    FILE *fp;
    fp = fopen( "database.txt", "w" );
    if ( fp == NULL ) //ellenorzi, hogy letezik-e a fajl.
    {
        printf( "Hiba a fajl megnyitsa kozben!\n" );
    }

    /*
    A tomb osszes nem ures elemenek kiirasa fajlba. A tomb elso ures karakteret az id mezoben egy 0 jelzi.
    */
    while( konyvek[konyvPoz].id != 0 )
    {
        fprintf( fp, "%d %s %s %d\n", konyvek[konyvPoz].id, konyvek[konyvPoz].cim, konyvek[konyvPoz].szerzo, konyvek[konyvPoz].ev );
        konyvPoz++;
    }
    fprintf( fp, "0" );
    fclose( fp );
}
