/*
    main.c
    Lefoglalja az adatok feldolgozasahoz szukseges tombot, illetve beolvassa egy szoveges fajlbol az adatokat es betolti azokat a tombbe.
    Elinditja a program kezelesehez szukseges menut, es a felhasznalo valasztasanak megfeleloen meghivja a megfelelo fuggveny(eke)t.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#include <stdio.h>
#include "customtypes.h"
#include "filehandler.h"
#include "arrayhandler.h"
#include "search.h"
#include "booksnum.h"


/*
    Feladata: A felhasznalo interakciojatol fuggoen adott fuggveny(ek) meghivasa.
    Hasznalatanak elofeltetelei: konyv tipus definialsa, futtatando fuggvenyek meglete.
    Bemeno parameterek: -
    Kimeno parameterek (visszateresi ertekek): a program helyes mukodese eseten 0.
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: valasz, len, konyvek[].
    Hivott fuggvenyek: file_Read(), file_write(), tomb_Listazas(), tomb_UjHozzaadas(), kereses_LegujabbKonyv(),
    kereses_LegregebbiKonyv(), kereses_CimSzerint(), kereses_SzerzoSzerint(), kereses_KiadasiEvSzerint(), konyvek_Szama(),
    printf(), scanf().

    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
int main()
{
    int valasz;
    int len = 1000;
    konyv konyvek[len];
    file_Read( konyvek );
    do
    {
        printf( "Könyvtári nyilvantarto rendszer\n" );
        printf( "|1| Adatok listazasa\n|2| Uj konyv bejegyzese\n|3| Legujabb megkeresese\n|4| Legregebbi megkeresese\n|5| Kereses cim szerint\n|6| Kereses szerzo szerint\n|7| Kereses a kiadas eve szerint\n|8| Konyvek szama\n|0| Kilepes\n" );
        scanf( "%d", &valasz );
        switch( valasz )
        {
        case(0):
            printf( "Kilepes..." );
            break;
        case(1):
            tomb_Listazas( konyvek, len );
            break;
        case(2):
            tomb_UjHozzadas( konyvek, len );
            file_Write( konyvek, len );
            break;
        case(3):
            kereses_LegujabbKonyv( konyvek, len );
            break;
        case(4):
            kereses_LegregibbKonyv( konyvek, len );
            break;
        case(5):
            kereses_CimSzerint( konyvek, len );
            break;
        case(6):
            kereses_SzerzoSzerint( konyvek, len );
            break;
        case(7):
            kereses_KiadasiEvSzerint( konyvek, len );
            break;
        case(8):
           printf( "Az adatbazisban talalhato konyvek szama: %d\n", konyvek_Szama( konyvek, len ));
            break;
        default:
            printf( "Nincs ilyen funkcio!\nAdjon meg egy szamot 1 es 8 kozott, vagy 0-t a kilepeshez.\n" );
        }
    }while( valasz != 0) ;

    return 0;
}
