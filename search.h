/*
    search.h
    A fajlban talalhato fuggvenyek keresest hajtanak vegre alkalmazva az eldontes, a kivalasztas, illetve a szelsoertek-kivalasztas
    teteleit.
    Laszlo Bence (Z43K7G)
    2016.11.22., 1.0
*/

#ifndef _SEARCH_H_
#define _SEARCH_H_

/*
    Feladata: egy konyv tipusu elemebol kiiratja a kepernyore a tomb ev mezojeben a legnagyobb szamot tartalmazo elem osszes mezojet.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb es elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    kereses_LegujabbKonyv( konyv *konyvek, int len );

/*
    Feladata: egy konyv tipusu elemebol kiiratja a kepernyore a tomb ev mezojeben a legkisebb szamot tartalmazo elem osszes mezojet.
    Hasznalatanak elofeltetelei: egy konyv tipusu tomb deklaralasa.
    Bemeno parameterek: egy konyv tipusu tomb es elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: -
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    kereses_LegregibbKonyv( konyv *konyvek, int len );

/*
    Feladata: egy konyv tipusu tombbol azon elemek kiiratasa a kepernyore, amelyek tartalmazzak a felhasznalo altal megadott
    karaktersorozatot a cim mezoben.

    Hasznalatanak elofeltetelei: egy konyv tipusu tomb dekralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: felhasznalo altal megadott karaktersorozat.
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: scanf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    kereses_CimSzerint( konyv *konyvek, int len );

/*
    Feladata: egy konyv tipusu tombbol azon elemek kiiratasa a kepernyore, amelyek tartalmazzak a felhasznalo altal megadott
    karaktersorozatot a szerzo mezoben.

    Hasznalatanak elofeltetelei: egy konyv tipusu tomb dekralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: felhasznalo altal megadott karaktersorozat.
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: scanf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    kereses_SzerzoSzerint( konyv *konyvek, int len );

/*
    Feladata: egy konyv tipusu tombbol azon elemek kiiratasa a kepernyore, amelyek ev mezojenek erteke megegyezik a felhasznalo altal
    megadott egesz szammal.

    Hasznalatanak elofeltetelei: egy konyv tipusu tomb dekralasa.
    Bemeno parameterek: egy konyv tipusu tomb, illetve elemeinek szama.
    Kimeno parameterek (visszateresi ertekek): -
    Egyeb parameterek: felhasznalo altal megadott egesz szam.
    Hivatkozott globalis valtozok: konyvek, len.
    Hivott fuggvenyek: scanf(), printf().
    Keszito: Laszlo Bence
    Utolso modositas datuma: 2016.11.22.
*/
    void
    kereses_KiadasiEvSzerint( konyv *konyvek, int len );
#endif
